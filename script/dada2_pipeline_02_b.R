library(dada2)
library(data.table)
library(doParallel)
library(magrittr)
cl <- makeCluster(6)
registerDoParallel(cl)

########## arguments
args <- commandArgs(TRUE)
min_read_nb <- args[1] %>% as.numeric
runs <- args[2] %>% strsplit(split=",") %>% unlist
runs <- c(paste(runs,"Cut1",sep="_"),paste(runs,"Cut2",sep="_"))
cat(runs)
#####

setwd("/projet/sbr/plancton-astan/work/nhenry/18SV4/")
getN <- function(x) sum(getUniques(x))
getA <- function(x) length(getUniques(x))

### selection based on the number of reads
tmp <- fread("dada2/log/filter.log")
tmpR <- tmpF <- tmp[reads.out>=min_read_nb,sample]

# File parsing
filtpathF <- "/projet/sbr/plancton-astan/work/nhenry/18SV4/FWD/filtered" 
filtpathR <- "/projet/sbr/plancton-astan/work/nhenry/18SV4/REV/filtered"
filtFsall <- paste(filtpathF,tmpF,sep="/")
filtRsall <- paste(filtpathR,tmpR,sep="/")
sample.names.all <- sub("_trimmed.+$","",basename(filtFsall))
sample.namesR.all <- sub("_trimmed.+$","",basename(filtRsall))
if(!identical(sample.names.all, sample.namesR.all)) stop("Forward and reverse files do not match.")
names(filtFsall) <- sample.names.all
names(filtRsall) <- sample.names.all
set.seed(100)

foreach(i=runs, .packages = c("data.table","dada2")) %dopar% {
  filtFs <- filtFsall[grep(i,names(filtFsall))]
  filtRs <- filtRsall[grep(i,names(filtRsall))]
  sample.names <- sub("_trimmed.+$","",basename(filtFs))
  # Learn forward error rates
  errF <- learnErrors(filtFs, nbases=1e8, multithread=F)
  pdf(paste0("dada2/log/errF_",i,".pdf"))
  print(plotErrors(errF, nominalQ=TRUE))
  dev.off()
  # Learn reverse error rates
  errR <- learnErrors(filtRs, nbases=1e8, multithread=F)
  pdf(paste0("dada2/log/errR_",i,".pdf"))
  print(plotErrors(errR, nominalQ=TRUE))
  dev.off()
  # Sample inference and merger of paired-end reads
  mergers <- vector("list", length(sample.names))
  names(mergers) <- sample.names
  track <- vector("list", length(sample.names))
  names(track) <- sample.names
  for(sam in sample.names) {
    cat("Processing:", sam, "\n")
    derepF <- derepFastq(filtFs[[sam]])
    ddF <- dada(derepF, err=errF, multithread=F,pool=T)
    derepR <- derepFastq(filtRs[[sam]])
    ddR <- dada(derepR, err=errR, multithread=F,pool=T)
    merger <- mergePairs(ddF, derepF, ddR, derepR,trimOverhang = T)
    mergers[[sam]] <- merger
    track[[sam]] <- data.table(sample=sam,denoisedF.read = getN(ddF),denoisedR.read = getN(ddR),merged.read = getN(merger),
                               denoisedF.seq = getA(ddF),denoisedR.seq = getA(ddR),merged.seq = getA(merger))
  }
  rm(derepF); rm(derepR)
  # Construct sequence table and remove chimeras
  track <- rbindlist(track)
  seqtab <- makeSequenceTable(mergers)
  saveRDS(track, paste0("dada2/log/track_",i,".rds")) # CHANGE ME to where you want sequence table saved
  saveRDS(seqtab, paste0("dada2/seqtab_",i,".rds")) # CHANGE ME to where you want sequence table saved
}

sessionInfo()
