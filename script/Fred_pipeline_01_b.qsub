#!/bin/bash
#$ -S /bin/bash
#$ -V
#$ -o Fred_pipeline_01.out
#$ -e Fred_pipeline_01.err
#$ -tc 50
#$ -sync no

MANIFEST="fastamanifest.txt"

read OUTPUT FORWARD1 FORWARD2 REVERSE1 REVERSE2 <<< $(awk "NR==$SGE_TASK_ID" "$MANIFEST")

FORWARD=$(mktemp --tmpdir="scratch/")
cat "${FORWARD1}" "${FORWARD2}" > ${FORWARD}
REVERSE=$(mktemp --tmpdir="scratch/")
cat "${REVERSE1}" "${REVERSE2}" > ${REVERSE}
TMP_FASTQ=$(mktemp --tmpdir="scratch/")
TMP_FASTQ2=$(mktemp --tmpdir="scratch/")
TMP_FASTA=$(mktemp --tmpdir="scratch/")
LOG="swarm/log/${OUTPUT}.log"
FINAL_FASTA="swarm/merged/${OUTPUT}.fas"
OUTPUT2="swarm/merged/${OUTPUT}.qualityfile"

# Merge read pairs
vsearch \
    --fastq_mergepairs "${FORWARD}" \
    --reverse "${REVERSE}" \
    --fastq_minovlen "${MIN_OVLEN}" \
    --fastq_ascii "${ENCODING}" \
    --fastqout "${TMP_FASTQ}" \
    --fastq_allowmergestagger \
    --quiet 2> ${LOG}
    
# Discard sequences containing Ns, add expected error rates
vsearch \
    --quiet \
    --fastq_filter "${TMP_FASTQ}" \
    --fastq_minlen "${MIN_LENGTH}" \
    --fastq_maxns 0 \
    --relabel_sha1 \
    --eeout \
    --fastqout "${TMP_FASTQ2}" 2>> "${LOG}"
    
# Discard sequences containing Ns, convert to fasta
vsearch \
    --quiet \
    --fastq_filter "${TMP_FASTQ}" \
    --fastq_minlen "${MIN_LENGTH}" \
    --fastq_maxns 0 \
    --fastaout "${TMP_FASTA}" 2>> "${LOG}"
    
# Dereplicate at the sample level
vsearch \
    --quiet \
    --derep_fulllength "${TMP_FASTA}" \
    --sizeout \
    --fasta_width 0 \
    --relabel_sha1 \
    --output "${FINAL_FASTA}" 2>> "${LOG}"
    
# Discard quality lines, extract hash, expected error rates and read length
sed 'n;n;N;d' "${TMP_FASTQ2}" | \
    awk 'BEGIN {FS = "[;=]"}
            {if (/^@/) {printf "%s\t%s\t", $1, $3} else {print length($1)}}' | \
    tr -d "@" |\
    sort -k3,3n -k1,1d -k2,2n | \
    uniq --check-chars=40 > "${OUTPUT2}"

rm -f "${TMP_FASTQ}" "${TMP_FASTQ2}" "${TMP_FASTA}" "${FORWARD}" "${REVERSE}"
