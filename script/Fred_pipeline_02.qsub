#!/bin/bash
#$ -S /bin/bash
#$ -V
#$ -o Fred_pipeline_02.out
#$ -e Fred_pipeline_02.err
#$ -pe thread 4

THREADS=4

echo -n "" > Fred_pipeline_02.out
echo -n "" > Fred_pipeline_02.err

# Produce the final quality file
find swarm/merged -name "*.qualityfile" -type f -exec cat {} + | \
    sort -k3,3n -k1,1d -k2,2n | \
    uniq --check-chars=40 > "swarm/merged/quality_file.txt"
# rm -f swarm/merged/*.qualityfile

grep step02 Fred_pipeline_parameters.tsv | awk '{print $2 "\t" $3}' > tmp

while read VAR_NAME VALUE; do
    declare "$VAR_NAME"=$VALUE
done < "tmp"

rm tmp

FASTA_FILES=$(find ./swarm/merged/ -name "*.fas" -type f ! -empty | \
                  tr "\n" " " | sed 's/\n$//')
N_SAMPLES=$(tr " " "\n" <<< ${FASTA_FILES} | wc -l)
FINAL_FASTA="swarm/${PROJECT}_${N_SAMPLES}_samples_${VERSION}.fas"
LOG="${FINAL_FASTA/.fas/.log}"
TMP_REPRESENTATIVES=$(mktemp --tmpdir="scratch/")
VSEARCH=$(which vsearch)
SWARM=$(which swarm)

## Global dereplication
echo "Dereplicating..."
cat ${FASTA_FILES} | \
    "${VSEARCH}" \
        --derep_fulllength - \
        --sizein \
        --sizeout \
        --fasta_width 0 \
        --output "${FINAL_FASTA}" 2> "${LOG}"

## Clustering
echo "Clustering..."

"${SWARM}" \
    -d 1 -f -t ${THREADS} -z \
    -i ${FINAL_FASTA/.fas/_1f.struct} \
    -s ${FINAL_FASTA/.fas/_1f.stats} \
    -w ${TMP_REPRESENTATIVES} \
    -o ${FINAL_FASTA/.fas/_1f.swarms} \
    ${FINAL_FASTA} 2>> "${LOG}"


## Sort representatives
"${VSEARCH}" \
    --fasta_width 0 \
    --quiet \
    --minsize 3 \
    --sortbysize ${TMP_REPRESENTATIVES} \
    --output ${FINAL_FASTA/.fas/_1f_representatives.fas} \
    --threads ${THREADS} 
    
rm ${TMP_REPRESENTATIVES}


## Chimera checking
echo "Chimera checking..."
REPRESENTATIVES=${FINAL_FASTA/.fas/_1f_representatives.fas}
UCHIME=${REPRESENTATIVES/.fas/.uchime}
"${VSEARCH}" \
    --uchime_denovo "${REPRESENTATIVES}" \
    --threads ${THREADS} \
    --uchimeout "${UCHIME}" 2>> "${LOG}"  
