library(data.table)
library(magrittr)
library(seqinr)
library(plyr)

args <- commandArgs(TRUE)
pr2_file <- args[2]
output <- args[3]

LCA_func <- function(X){
  X <- unique(X)
  if(length(X)>1){
    x <- strsplit(X,split="\\|") %>%
      lapply(.,function(Y) matrix(Y) %>% t %>% data.table) %>%
      rbindlist(fill=T) %>%
      as.matrix
    
    i <- 0
    while(length(unique(x[,i+1])) == 1  & i <= (ncol(x))-1){
      i <- i+1
    }
    
    if(i > 0){
      x[1,1:i] %>% paste(collapse = "|")
    }else{
      "Unknown"
    }
  }else{
    X
  }
}

## pr2
references <- read.fasta(pr2_file)
references <- getAnnot(references)
head(references)

tmp <- lapply(1:length(references), function(i) {
  gsub("^>|[A-Z]+\\|tax=k\\:","",references[[i]]) %>%
    gsub(",\\w\\:","|",.) %>%
    strsplit(" ") %>% unlist
})

references <- matrix(unlist(tmp), ncol=2, byrow=TRUE) %>%
  data.table

setnames(references,c("id","lineage"))

###### 
annot <- fread(args[1],header=F)
setnames(annot,c("amplicon","identity","id"))
corresp <- fread(args[5])
annot <- merge(annot,corresp,by.x="amplicon",by.y="asv")
annot <- merge(references,annot,by="id")

tmp <- lapply(unique(annot[,amplicon]), function(i) {
  data.table(i,LCA_func(annot[amplicon==i,lineage]))
})

annot <- merge(annot[,paste(id,collapse=","),by=list(amplicon,identity,sequence)],rbindlist(tmp),by.x="amplicon",by.y="i")

fwrite(annot,paste0(output,"_",args[4]),sep="\t",row.names=F,col.names = F,quote = F,na="NA")
