library(data.table)
library(magrittr)

sessionInfo()
### arguments

args <- commandArgs(TRUE)
trunc_fwd <- args[1] %>% as.numeric
trunc_rev <- args[2] %>% as.numeric
maxee <- args[3] %>% as.numeric
fastqFs <- fastqRs <- args[4]

setwd("/projet/sbr/plancton-astan/work/nhenry/18SV4/")
# File parsing
pathF <- "/projet/sbr/plancton-astan/work/nhenry/18SV4/FWD"
pathR <- "/projet/sbr/plancton-astan/work/nhenry/18SV4/REV"
filtpathF <- file.path(pathF, "filtered")
filtpathR <- file.path(pathR, "filtered")

if(length(fastqFs) != length(fastqRs)) stop("Forward and reverse files do not match.")

# Filtering: THESE PARAMETERS ARENT OPTIMAL FOR ALL DATASETS
dada2::filterAndTrim(fwd=file.path(pathF, fastqFs), filt=file.path(filtpathF, fastqFs),
              rev=file.path(pathR, fastqRs), filt.rev=file.path(filtpathR, fastqRs), truncLen = c(trunc_fwd,trunc_rev),
              maxEE=maxee, maxN=0, compress=TRUE, verbose=TRUE, multithread=TRUE) -> out

data.table(out,keep.rownames = T) %>% fwrite(paste0("dada2/log/",fastqFs,".filter.log"),sep=" ",col.names=F)
