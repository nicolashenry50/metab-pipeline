---
title: "Astan pipeline"
author: "Nicolas Henry"
date: "`r Sys.Date()`"
output:
  html_document:
    fig_retina: NULL
    toc: true
    toc_float: true
    code_folding: hide
---


```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
knitr::opts_knit$set(root.dir = '/home/nicolas/Documents/Astan/cluster/18SV4')
```



```{r,echo=FALSE}
library(data.table)
library(magrittr)
library(ggplot2)
library(ggpubr)
```

```{r}
subset_code <- function(X){
  X <- X[grep("(\\#\\$ \\-(wd|m|M|q))",X,invert = T)]
  sub("/projet/sbr/tara/scratch/nhenry/?","scratch/",X)
}
```

# Introduction
This document summarizes the pipeline I used to generate OTU/ASV tables. The main idea of this pipeline is to produce two OTU/ASV tables, one using dada2 and an other one using Swarm + VSEARCH. The dada2 section of the pipeline is adapted from the dada2 [big data tutorial](https://benjjneb.github.io/dada2/bigdata_paired.html) and the Swarm + VSEARCH section is adapted from [Fred's metabarcoding pipeleine](https://github.com/frederic-mahe/swarm/wiki/Fred's-metabarcoding-pipeline).

As an example, the rDNA 18S V4 metabarcoding data of the time series from Astan are processed. 

```{r, fig.width=10,fig.height=12}
DiagrammeR::mermaid('graph TD
    D0["FastQ files from sequencing (R1 and R2 files)"] -->  S1
    subgraph ""
    S1["Check quality encoding"] --> S2["Demultiplexing and primer trimming"]
    S2 --> S3["Quality plots"]
    end
    S3 --> D1["FWD and REV primer trimmed sequence for each sample"]
    D1 -->|DADA2| Da1["Filtration and length trimming"]
    D1 -->|Fred\'s pipeline| Sw1["Merging"]
    subgraph ""
    Da1 --> Da2["Denoising and merging"]
    Da2 --> Da3["Chimera detection, OTU table construction"]
    end
    Da3 -->|Unique sequences| Z1["Taxonomic assignment"]
    subgraph ""
    Sw1 --> Sw3["Swarm and chimera detection"]
    Sw3 --> Sw4["OTU table construction"]
    Sw4 --> Sw5["OTU table filtration"]
    end
    Sw5 -->|Unique sequences| Z1["Taxonomic assignment"]
    Z1 --> Z2["Final dada2 ASV table"]
    Z1 --> Z3["Final swarm OTU table"]')
```

# Check quality encoding
This script checks for each run how the quality is [encoded](https://en.wikipedia.org/wiki/FASTQ_format#Encoding). This information is needed for Fred's pipeline.

```{r results='asis',echo=FALSE}
tmp <- subset_code(readLines('v202009/script/A00_encoding_check.qsub'))
cat(tmp, sep = '\n',file = "Full_pipeline/script/A00_encoding_check.qsub")
cat(c("``` bash",tmp,"```"), sep = '\n')
```

# Demultiplexing and primer trimming
Demultiplexing and primer trimming are done using Cutadapt. For most of the fastq files processed here, the reads are in mixed orientation. So Cutadapt is used twice, first looking for tags and forward primers in R1 and then looking for tags and forward primers in R2.

To do so, a text file in which are listed the samples, corresponding tags, and the raw fastq file to process is first created (manifest.txt) by the script below.

```{r results='asis',echo=FALSE}
tmp <- subset_code(readLines('v202009/script/A01_a.qsub'))
cat(tmp, sep = '\n',file = "Full_pipeline/script/A01_a.qsub")
cat(c("``` bash",tmp,"```"), sep = '\n')
```

This is an overview of the file. It contains in total `r fread("/home/nicolas/Documents/Astan/cluster/18SV4/v202009/manifest.txt",header = F) %>% nrow` lines, one per sample.

```{r,echo=FALSE}
fread("/home/nicolas/Documents/Astan/cluster/18SV4/v202009/manifest.txt",header = F) %>% head %>% knitr::kable()
```

Then the following script is applied to each line (`$SGE_TASK_ID`) of this file. For each sample:

* The results of demultiplexing alone are saved into `${SAMPLE}_${RUNID}_R1.fastq.gz` and `${SAMPLE}_${RUNID}_R2.fastq.gz`. The reads are not reorientated. These files are deposited in public repositories.
* The results of primer trimming are saved into `FWD/${SAMPLE}_${RUNID}_Cut1_trimmed.fastq.gz`, `FWD/${SAMPLE}_${RUNID}_Cut2_trimmed.fastq.gz`, `REV/${SAMPLE}_${RUNID}_Cut1_trimmed.fastq.gz` and `REV/${SAMPLE}_${RUNID}_Cut2_trimmed.fastq.gz`. The forward reads are placed in the FWD/ folder, those extracted from the R1 file are named Cut1 and those extracted from the R2 file are named Cut2. The reverse files are placed in the REV/ folder and named accordingly to the forward reads.

```{r results='asis',echo=FALSE}
tmp <- subset_code(readLines('v202009/script/A01_b.qsub'))
cat(tmp, sep = '\n',file = "Full_pipeline/script/A01_b.qsub")
cat(c("``` bash",tmp,"```"), sep = '\n')
```

# Quality plots
Plots summarizing the quality of the reads are generated at sample, run and study scale. These plots allow us to define where the quality of the sequences drop.

```{r results='asis',echo=FALSE}
tmp <- subset_code(readLines('v202009/script/A02_a.qsub'))
cat(tmp, sep = '\n',file = "Full_pipeline/script/A02_a.qsub")
cat(c("``` bash",tmp,"```"), sep = '\n')
```

```{r results='asis',echo=FALSE}
tmp <- subset_code(readLines('v202009/script/A02_b.R'))
cat(tmp, sep = '\n',file = "Full_pipeline/script/A02_b.R")
cat(c("``` bash",tmp,"```"), sep = '\n')
```

This is the figure produced at the study scale for the forward reads:

![](qplot.png "Quality plot")

# Fred's pipeline

## Parameters
Some parameters are stored in a tsv file. It allows tuning these parameters without having to change the scripts. The pipeline is organized into three steps:

* step01: merging and dereplication of the reads
* step02: clustering using Swarm
* step03: OTU table construction 

```{r,echo=FALSE}
fread("/home/nicolas/Documents/Astan/cluster/18SV4/v202009/Fred_pipeline_parameters.tsv") %>% knitr::kable()
system("cp Fred_pipeline_parameters.tsv Full_pipeline/script/")
```

## Merging and dereplication (step 1)
The sequences are merged and dereplicated using VSEARCH. For each sample, the four files to work with are listed:

```{r results='asis',echo=FALSE}
tmp <- subset_code(readLines('v202009/script/Fred_pipeline_01_a.qsub'))
cat(tmp, sep = '\n',file = "Full_pipeline/script/Fred_pipeline_01_a.qsub")
cat(c("``` bash",tmp,"```"), sep = '\n')
```

Forward and reverse reads with at least 10 nucleotides overlap are merged. Merged reads without Ns and at least 200 bp long are saved in a fasta file.

```{r results='asis',echo=FALSE}
tmp <- subset_code(readLines('v202009/script/Fred_pipeline_01_b.qsub'))
cat(tmp, sep = '\n',file = "Full_pipeline/script/Fred_pipeline_01_b.qsub")
cat(c("``` bash",tmp,"```"), sep = '\n')
```


## Swarm clustering (step 2)
The sequences are dereplicated at the project level and clustered using Swarm with d=1 and the *fastidious* option. A representative sequence is defined for each cluster/OTU (the one with the highest number of reads).

```{r results='asis',echo=FALSE}
tmp <- subset_code(readLines('v202009/script/Fred_pipeline_02.qsub'))
cat(tmp, sep = '\n',file = "Full_pipeline/script/Fred_pipeline_02.qsub")
cat(c("``` bash",tmp,"```"), sep = '\n')
```

## OTU table construction (step 3)
Finally, the OTU table was assembled using the different files generated during step 1 and 2. The OTU table is filtered in order to keep non chimeric OTUs, with a quality per base superior to 0.0002 and which contain at least three reads in at least two samples. The assemblage is done with a python script.

```{r results='asis',echo=FALSE}
tmp <- subset_code(readLines('v202009/script/Fred_pipeline_03_a.qsub'))
cat(tmp, sep = '\n',file = "Full_pipeline/script/Fred_pipeline_03_a.qsub")
cat(c("``` bash",tmp,"```"), sep = '\n')
```

The python script:

```{r results='asis',echo=FALSE}
tmp <- subset_code(readLines('v202009/script/Fred_pipeline_03_b.py'))
cat(tmp, sep = '\n',file = "Full_pipeline/script/Fred_pipeline_03_b.py")
cat(c("``` bash",tmp,"```"), sep = '\n')
```

# Dada2 pipeline
## Parameters
Some parameters are stored in a tsv file. It allows tuning these parameters without having to change the scripts. The pipeline is organized into three steps:

* step01: filtration and length trimming
* step02: denoising and merging
* step03: chimera detection and OTU table construction

```{r,echo=FALSE}
fread("/home/nicolas/Documents/Astan/cluster/18SV4/v202009/dada2_pipeline_parameters.tsv") %>% knitr::kable()
system("cp dada2_pipeline_parameters.tsv Full_pipeline/script/")
```

## Filtration and length trimming (step 1)
This step of filtration is done by the use of the function filterAndTrim() from the library dada2 with standard filtering parameters. The truncation length for both forward and reverse reads is high in order to allow to recover long 18S V4 sequences.
```{r results='asis',echo=FALSE}
tmp <- subset_code(readLines('v202009/script/dada2_pipeline_01_a.qsub'))
cat(tmp, sep = '\n',file = "Full_pipeline/script/dada2_pipeline_01_a.qsub")
cat(c("``` bash",tmp,"```"), sep = '\n')
```

```{r results='asis',echo=FALSE}
tmp <- subset_code(readLines('v202009/script/dada2_pipeline_01_b.qsub'))
cat(tmp, sep = '\n',file = "Full_pipeline/script/dada2_pipeline_01_b.qsub")
cat(c("``` bash",tmp,"```"), sep = '\n')
```

```{r results='asis',echo=FALSE}
tmp <- subset_code(readLines('v202009/script/dada2_pipeline_01_c.R'))
cat(tmp, sep = '\n',file = "Full_pipeline/script/dada2_pipeline_01_c.R")
cat(c("``` bash",tmp,"```"), sep = '\n')
```

## Denoising and merging (step 2)
After learning the error rate of each run, the core algorithm of dada2 is applied to infer the true sequence variants. For each run, the samples are pooled together with the argument pool=TRUE which can increase sensitivity to rare variants. Then the forward and reverse sequences are merged together.
```{r results='asis',echo=FALSE}
tmp <- subset_code(readLines('v202009/script/dada2_pipeline_02_a.qsub'))
cat(tmp, sep = '\n',file = "Full_pipeline/script/dada2_pipeline_02_a.qsub")
cat(c("``` bash",tmp,"```"), sep = '\n')
```

```{r results='asis',echo=FALSE}
tmp <- subset_code(readLines('v202009/script/dada2_pipeline_02_b.R'))
cat(tmp, sep = '\n',file = "Full_pipeline/script/dada2_pipeline_02_b.R")
cat(c("``` bash",tmp,"```"), sep = '\n')
```

## Chimera detection and ASV table construction (step 3)
The last step before building the ASV table is to remove chimeras. This is done with the removeBimeraDenovo() function. The statistics about the pipeline are put together into one file and the ASV is built.
```{r results='asis',echo=FALSE}
tmp <- subset_code(readLines('v202009/script/dada2_pipeline_03_a.qsub'))
cat(tmp, sep = '\n',file = "Full_pipeline/script/dada2_pipeline_03_a.qsub")
cat(c("``` bash",tmp,"```"), sep = '\n')
```

```{r results='asis',echo=FALSE}
tmp <- subset_code(readLines('v202009/script/dada2_pipeline_03_b.R'))
cat(tmp, sep = '\n',file = "Full_pipeline/script/dada2_pipeline_03_b.R")
cat(c("``` bash",tmp,"```"), sep = '\n')
```

# Taxonomic assignment
At this stage, we have one OTU/ASV table for each pipeline, but the sequences are not yet annotated. Since all lot of sequences will be shared between the two pipelines, the taxonomic assignment is done for both at the same time.

The fasta file containing the sequences from both pipeline is first split into several chunks in order to run the assignment in parallel.

```{r results='asis',echo=FALSE}
tmp <- subset_code(readLines('v202009/script/Z01_a.qsub'))
cat(tmp, sep = '\n',file = "Full_pipeline/script/Z01_a.qsub")
cat(c("``` bash",tmp,"```"), sep = '\n')
```

The V4 region was extracted from the 18S rDNA references sequences of [PR2](https://pr2-database.org/) v4.12 with Cutadapt using the same primer pair as the one for the amplification (maximum error rate of 0.2 and minimum overlap of 2/3 the length of the primer).

``` bash
#!/bin/bash

INPUT="pr2_version_4.12.0_18S_UTAX.fasta.gz"

# Define variables and output files
OUTPUT="${INPUT/.fasta.gz/_V4.fasta}"
LOG="${INPUT/.fasta.gz/_V4.log}"
PRIMER_F="CCAGCASCYGCGGTAATTCC"
PRIMER_R="ACTTTCGTTCTTGATYRA"
ANTI_PRIMER_R="TYRATCAAGAACGAAAGT"
MIN_LENGTH=32
MIN_F=$(( ${#PRIMER_F} * 2 / 3 ))
MIN_R=$(( ${#PRIMER_R} * 2 / 3 ))
CUTADAPT="cutadapt --discard-untrimmed --minimum-length ${MIN_LENGTH}"

# Trim forward & reverse primers, format
zcat "${INPUT}" | sed '/^>/ ! s/U/T/g' | \
     ${CUTADAPT} -g "${PRIMER_F}" -O "${MIN_F}" -e 0.2 - 2> "${LOG}" | \
     ${CUTADAPT} -a "${ANTI_PRIMER_R}" -O "${MIN_R}" -e 0.2 - 2>> "${LOG}" | \
     sed '/^>/ s/;/|/g ; /^>/ s/ /_/g ; /^>/ s/_/ /1' > "${OUTPUT}"
```


The representative sequences of each OTU were compared to these V4 reference sequences by pairwise global alignment (usearch_global VSEARCH’s command). The OTU inherits the taxonomy of the best hit.

```{r results='asis',echo=FALSE}
tmp <- subset_code(readLines('v202009/script/Z01_b.qsub'))
cat(tmp, sep = '\n',file = "Full_pipeline/script/Z01_b.qsub")
cat(c("``` bash",tmp,"```"), sep = '\n')
```

In the case of several best hits (ties), the last common ancestor of the references is used as taxonomic assignment. For example, the last common ancestor of *Dinophyceae|Amphidomataceae|g:Amphidoma|Amphidoma+languida* and *Dinophyceae|Amphidomataceae|g:Azadinium|Azadinium+caudatum* is *Dinophyceae|Amphidomataceae*. The R code below is doing the job:

```{r results='asis',echo=FALSE}
tmp <- subset_code(readLines('v202009/script/Z01_c.R'))
cat(tmp, sep = '\n',file = "Full_pipeline/script/Z01_c.R")
cat(c("``` bash",tmp,"```"), sep = '\n')
```

This last piece of code concatenates the results previously obtained for each chunk.

```{r results='asis',echo=FALSE}
tmp <- subset_code(readLines('v202009/script/Z02.qsub'))
cat(tmp, sep = '\n',file = "Full_pipeline/script/Z02.qsub")
cat(c("``` bash",tmp,"```"), sep = '\n')
```

# Final OTU table
The OTU table and the taxonomy are merged together. Some samples have been sequenced several times, in that case the read set with the highest number of reads is retained.

```{r, eval=F}
library(data.table)
library(magrittr)

### swarm 
data <- fread("/home/nicolas/Documents/Astan/cluster/18SV4/v202009/ASTAN_18SV4_407_samples_v202009.OTU.filtered.table")
taxo <- fread("/home/nicolas/Documents/Astan/cluster/18SV4/v202009/ASTAN_18SV4.taxo")
setnames(taxo,c("asv","identity","sequence","references","taxonomy"))
taxo[,asv:=NULL]
data <- merge(taxo,data,by="sequence",all.y = T)

samples <- data.table(grep("RA",colnames(data),value=T))

samples[,envsple:=sub("(^[^_]+)[_](\\d+).+$","\\1_\\2",V1)]
samples[,envsple] %>% unique
dup_samples <- samples[,paste(V1,collapse=" ; "),by=envsple][grep(";",V1)]

x <- lapply(dup_samples[,V1],function(X){
  tmp <- strsplit(X,split = " ; ") %>% unlist
  sapply(tmp,function(Y) data[,sum(get(Y))]) %>% paste(collapse = " ; ")
}) %>% unlist

dup_samples[,nreads:=x]
fwrite(dup_samples,"/home/nicolas/Documents/Astan/cluster/18SV4/v202009/duplicated_samples.tsv",sep="\t")

sple_remove <- dup_samples[,V1] %>%
  strsplit(split=" ; ") %>%
  lapply(.,function(X){
    test1 <- grepl("GYJ$",X)
    test2 <- grepl("JGT\\-1$",X)
    if(sum(test1)>0){
      X[!test1]
    }else if(sum(test2)>0){
      X[test2]
    }
  }) %>% unlist

x <- ! colnames(data) %in% sple_remove
data <- data[,.SD,.SDcols=x]


samples <- data.table(grep("RA",colnames(data),value=T))
samples[,V2:=sub("(^[^_]+)[_](\\d+).+$","\\1_\\2",V1)]

samples[,nreads:=sapply(V1,function(X) data[,sum(get(X))])]
setnames(samples,c("seqfile","sample","nreads"))

setnames(data,samples[,seqfile],samples[,sample])
data[order(total,decreasing = T)][,list(taxonomy,total)]

x <- rowSums(data[,.SD,.SDcols=grep("^RA",colnames(data))])
data[,total:=x]
x <- apply(data[,.SD,.SDcols=grep("^RA",colnames(data))],1,function(X) sum(X>0))
data[,spread:=x]
data <- data[total>2&spread>1][order(total,decreasing = T)]

fwrite(samples,"~/data/Astan/ASTAN_18SV4_407_samples_v202009.sample.list",sep="\t",na="NA")
fwrite(data,"~/data/Astan/ASTAN_18SV4_407_samples_v202009.OTU.filtered.table.subseted",sep="\t",na="NA")

### dada2

data <- fread("seqtab_all.tsv")
tmp <- grep("RA",colnames(data),value=T) %>%
  sub("_Cut\\d$","",.) %>%
  unique

for(i in tmp){
  x <- grep(i,colnames(data),value=T)
  data[,(i):=rowSums(.SD),.SDcols=x]
  data[,(x):=NULL]
}

taxo <- fread("ASTAN_18SV4.taxo")
setnames(taxo,c("asv","identity","sequence","references","taxonomy"))
taxo[,asv:=NULL]
taxo <- unique(taxo)
data <- merge(taxo,data,by="sequence",all.y = T)

samples <- data.table(grep("RA",colnames(data),value=T))

samples[,envsple:=sub("(^[^_]+)[_](\\d+).+$","\\1_\\2",V1)]
samples[,envsple] %>% unique
dup_samples <- samples[,paste(V1,collapse=" ; "),by=envsple][grep(";",V1)]

x <- lapply(dup_samples[,V1],function(X){
  tmp <- strsplit(X,split = " ; ") %>% unlist
  sapply(tmp,function(Y) data[,sum(get(Y))]) %>% paste(collapse = " ; ")
}) %>% unlist

dup_samples[,nreads:=x]
fwrite(dup_samples,"dada_duplicated_samples.tsv",sep="\t")

sple_remove <- dup_samples[,V1] %>%
  strsplit(split=" ; ") %>%
  lapply(.,function(X){
    test1 <- grepl("GYJ$",X)
    test2 <- grepl("JGT\\-1$",X)
    if(sum(test1)>0){
      X[!test1]
    }else if(sum(test2)>0){
      X[test2]
    }
  }) %>% unlist

x <- ! colnames(data) %in% sple_remove
data <- data[,.SD,.SDcols=x]


samples <- data.table(grep("RA",colnames(data),value=T))
samples[,V2:=sub("(^[^_]+)[_](\\d+).+$","\\1_\\2",V1)]

samples[,nreads:=sapply(V1,function(X) data[,sum(get(X))])]
setnames(samples,c("seqfile","sample","nreads"))

setnames(data,samples[,seqfile],samples[,sample])

x <- rowSums(data[,.SD,.SDcols=grep("^RA",colnames(data))])
data[,total:=x]
x <- apply(data[,.SD,.SDcols=grep("^RA",colnames(data))],1,function(X) sum(X>0))
data[,spread:=x]
data <- data[total>2&spread>1][order(total,decreasing = T)]
data <- data[,.SD,.SDcols=c("amplicon","sequence","taxonomy","identity","references","total","spread",sort(grep("^RA",colnames(data),value=T)))]

data[,length(amplicon)]
data[,length(unique(amplicon))]
data[order(total,decreasing = T)][,list(taxonomy,total)][1:20]
data[is.na(taxonomy)]
fwrite(samples,"~/data/Astan/ASTAN_18SV4_dada2_v202009.sample.list",sep="\t",na="NA")
fwrite(data,"~/data/Astan/ASTAN_18SV4_dada2_v202009.filtered.table.subseted",sep="\t",na="NA")
```

# OTU table statistics
## Swarm table
### General numbers

```{r}
data <- fread("~/data/Astan/ASTAN_18SV4_407_samples_v202009.OTU.filtered.table.subseted")
samples <- fread("~/data/Astan/ASTAN_18SV4_407_samples_v202009.sample.list")
samples[,c("sample.event","fraction","run"):=tstrsplit(seqfile,split="_")]
```

The final OTU table contains `r nrow(data)` OTUs found in `r length(grep("RA",names(data)))` samples with `r samples[,sum(nreads)]` reads. The distribution of the number of reads per run and size fraction is represented below with the use of boxplots:

```{r, fig.width=6,fig.align="center",fig.height=5,warning=F}
ggplot(samples,aes(x=run,y=nreads))+
  geom_boxplot(aes(fill=fraction))+
  # geom_hline(yintercept = 1000,colour="red")+
  scale_y_continuous("# reads")+
  theme_classic()
```

### Taxonomic assignment

```{r}
tmp <- data[,list(supergroup=sub("^Eukaryota\\|([^\\|]+).*$","\\1",taxonomy)),by=list(amplicon,identity,total)]
tmp[supergroup=="Eukaryota",supergroup:="Eukaryota_X"]
```

`r round(tmp[!is.na(identity),length(amplicon)]/nrow(tmp)*100,digits=2)`% of the OTUs and `r round(tmp[!is.na(identity),sum(total)]/tmp[,sum(total)]*100,digits=2)`% of the reads are assigned to a reference sequence with a score of similarity >=50%. The majority of the assigned OTUs (`r round(tmp[identity>95,length(amplicon)]/nrow(tmp[!is.na(identity)])*100,digits=2)`%) are assigned with more than 95% of identity. 

```{r, fig.width=8,fig.align="center",fig.height=4,warning=F}
p1 <- ggplot(tmp,aes(x=identity))+
  geom_bar(aes(fill=supergroup))+
  scale_fill_brewer(palette="Set3")+
  scale_x_binned("% identity")+
  scale_y_continuous("# OTUs")+
  theme_classic()

p2 <- ggplot(tmp,aes(x=identity))+
  geom_bar(aes(fill=supergroup,weight=total))+
  scale_fill_brewer(palette="Set3")+
  scale_x_binned("% identity")+
  scale_y_continuous("# reads")+
  theme_classic()

ggarrange(p1,p2,nrow=1,common.legend = T,legend = "bottom")
```

The majority of the reads (`r round(tmp[identity>95,sum(total)]/tmp[!is.na(identity),sum(total)]*100,digits=2)`%) are assigned with more than 99.5% of identity.

```{r, fig.width=8,fig.align="center",fig.height=4,warning=F}
p1 <- ggplot(tmp[identity>95],aes(x=identity))+
  geom_bar(aes(fill=supergroup))+
  scale_fill_brewer(palette="Set3")+
  scale_x_binned("% identity")+
  scale_y_continuous("# OTUs")+
  theme_classic()

p2 <- ggplot(tmp[identity>95],aes(x=identity))+
  geom_bar(aes(fill=supergroup,weight=total))+
  scale_fill_brewer(palette="Set3")+
  scale_x_binned("% identity")+
  scale_y_continuous("# reads")+
  theme_classic()

ggarrange(p1,p2,nrow=1,common.legend = T,legend = "bottom")
```

## Dada2 table
### General numbers

```{r}
data <- fread("~/data/Astan/ASTAN_18SV4_dada2_v202009.filtered.table.subseted.gz")
samples <- fread("~/data/Astan/ASTAN_18SV4_dada2_v202009.sample.list")
samples[,c("sample.event","fraction","run"):=tstrsplit(seqfile,split="_")]
```

The final OTU table contains `r nrow(data)` OTUs found in `r length(grep("RA",names(data)))` samples with `r samples[,sum(nreads)]` reads. The distribution of the number of reads per run and size fraction is represented below with the use of boxplots:

```{r, fig.width=6,fig.align="center",fig.height=5,warning=F}
ggplot(samples,aes(x=run,y=nreads))+
  geom_boxplot(aes(fill=fraction))+
  # geom_hline(yintercept = 1000,colour="red")+
  scale_y_continuous("# reads")+
  theme_classic()
```

### Taxonomic assignment

```{r}
tmp <- data[,list(supergroup=sub("^Eukaryota\\|([^\\|]+).*$","\\1",taxonomy)),by=list(amplicon,identity,total)]
tmp[supergroup=="Eukaryota",supergroup:="Eukaryota_X"]
```

`r round(tmp[!is.na(identity),length(amplicon)]/nrow(tmp)*100,digits=2)`% of the OTUs and `r round(tmp[!is.na(identity),sum(total)]/tmp[,sum(total)]*100,digits=2)`% of the reads are assigned to a reference sequence with a score of similarity >=50%. The majority of the assigned OTUs (`r round(tmp[identity>95,length(amplicon)]/nrow(tmp[!is.na(identity)])*100,digits=2)`%) are assigned with more than 95% of identity. 

```{r, fig.width=8,fig.align="center",fig.height=4,warning=F}
p1 <- ggplot(tmp,aes(x=identity))+
  geom_bar(aes(fill=supergroup))+
  scale_fill_brewer(palette="Set3")+
  scale_x_binned("% identity")+
  scale_y_continuous("# OTUs")+
  theme_classic()

p2 <- ggplot(tmp,aes(x=identity))+
  geom_bar(aes(fill=supergroup,weight=total))+
  scale_fill_brewer(palette="Set3")+
  scale_x_binned("% identity")+
  scale_y_continuous("# reads")+
  theme_classic()

ggarrange(p1,p2,nrow=1,common.legend = T,legend = "bottom")
```

The majority of the reads (`r round(tmp[identity>95,sum(total)]/tmp[!is.na(identity),sum(total)]*100,digits=2)`%) are assigned with more than 99.5% of identity.

```{r, fig.width=8,fig.align="center",fig.height=4,warning=F}
p1 <- ggplot(tmp[identity>95],aes(x=identity))+
  geom_bar(aes(fill=supergroup))+
  scale_fill_brewer(palette="Set3")+
  scale_x_binned("% identity")+
  scale_y_continuous("# OTUs")+
  theme_classic()

p2 <- ggplot(tmp[identity>95],aes(x=identity))+
  geom_bar(aes(fill=supergroup,weight=total))+
  scale_fill_brewer(palette="Set3")+
  scale_x_binned("% identity")+
  scale_y_continuous("# reads")+
  theme_classic()

ggarrange(p1,p2,nrow=1,common.legend = T,legend = "bottom")
```

# Versions
## Tools
* cutadapt version 2.8 (conda environment with Python version 3.7.9)
* vsearch version 2.9.1
* Swarm version 2.2.2
* R version 3.4.4
* Python version 2.7.2
* GNU Awk version 3.1.7
* GNU bash version 4.1.2

## Databases
* PR2 version 4.12